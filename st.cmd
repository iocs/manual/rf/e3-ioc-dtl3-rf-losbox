require essioc
require iocmetadata
require lobox

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet("DEVICENAME", "DTL-030:RFS-LOSBox-010")
epicsEnvSet("IPADDR", "dtl3-lod-001.tn.esss.lu.se")

iocshLoad("$(lobox_DIR)/lodbox.iocsh")

pvlistFromInfo("ARCHIVE_THIS", "$(DEVICENAME):ArchiverList")
pvlistFromInfo("SAVRES_THIS", "$(DEVICENAME):SavResList")
